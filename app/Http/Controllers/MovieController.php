<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Movie::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Movie::create([
                'name' => $request->name,
                'active' => $request->active,
            ]);
            return response()->json(['response' => 'Guardado Exitosamente', 'status' => 1]);
        } catch (QueryException $th) {
            Log::emergency($th);
            return response()->json(['response' => 'Error al Guardar', 'status' => 2]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(Movie::whereId($id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $type = Movie::findOrFail($request->id);
            $type->name = $request->name;
            $type->active = $request->active;
            $type->save();
            return response()->json(['response' => 'Actualizado Exitosamente', 'status' => 1]);
        } catch (QueryException $th) {
            Log::emergency($th);
            return response()->json(['response' => 'Error al Actualizar', 'status' => 2]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $type = Movie::findOrFail($id);
            $type->delete();
            return response()->json(['response' => 'Eliminado Exitosamente', 'status' => 1]);
        } catch (QueryException $th) {
            Log::emergency($th);
            return response()->json(['response' => 'Error al Eliminar', 'status' => 2]);
        }
    }
}
